$(function(){
	function initHighlighter(){
		if($('.uncurtain'))
		{
			//$('.head_nav > table > tbody > tr').append('<td><nobr><a class="top_nav_link uncurtain">Раздвинуть ноги</a></nobr></td>');
			loadHighlighter();
		}
	}

	VK_HL_LANGUAGES = {'cpp' : /\[c\](.*?)\[\/c\]/, 'javascript': /\[js\](.*?)\[\/js\]/, 'php': /\[php\](.*?)\[\/php\]/};
	VK_HL_SHORTCODES = ['[c]', '[php]', '[js]'];

	function loadHighlighter()
	{
		if(document.URL.indexOf('sel=') > 0)
		{
			$('.im_msg_text:not(.non-script)').each(function(){
				var html = $(this).html(),
					boolFound = false,
					matches = [],
					type = false;

				for( var i = 0; i < 3; i++ )
				{
					if(html.indexOf(VK_HL_SHORTCODES[i])+1)
					{
						boolFound = true;
						break;
					}
				}

				if(!boolFound)
				{
					$(this).addClass('non-script');
					return;
				}

				$(this).find('br').remove();
				html = $(this).html();

				for( lang in VK_HL_LANGUAGES )
				{
					if(matches = html.match(VK_HL_LANGUAGES[lang]))
					{
						type = lang;
						break;
					}
				}

				if(matches)
				{
					code = matches[1].replace(/&lt;/g,'<').replace(/&gt;/g,'>');
					var uid = 'ace_'+MD5(matches[0]);
					$(this).html("<pre id='"+uid+"' style='overflow-x: auto;'>"+js_beautify(code).replace(/ /g,'&nbsp;')+"</pre>");
					$('#'+uid).snippet(type,{style:"golden"});
					$('#'+uid).parent().css({'overflow-x':'auto'});
				}
			})
		}

		setTimeout(function(){loadHighlighter()}, 100);
	}

	initHighlighter();
})